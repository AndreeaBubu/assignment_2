const FIRST_NAME = "Andreea";
const LAST_NAME = "Văcaru";
const GRUPA = "1085";

/**
 * Make the implementation here
 */
function initCaching() {
  var cacheObj = new Object();

  cacheObj.pageAccessCounter = function(websiteSection) {
    //daca sectiunea e home sau nu are parametru
    if (websiteSection === "home" || typeof websiteSection === "undefined") {
      if (!cacheObj["home"]) {
        cacheObj["home"] = 1;
      } else {
        cacheObj["home"]++;
      }
    } //daca sectiunea e contact
    else if (websiteSection === "contact") {
      if (!cacheObj["contact"]) {
        cacheObj["contact"] = 1;
      } else {
        cacheObj["contact"]++;
      }
    } //daca sectiunea e about
    else if (websiteSection === "about") {
      if (!cacheObj["about"]) {
        cacheObj["about"] = 1;
      } else {
        cacheObj["about"]++;
      }
    }
    //daca sectiunea nu e scrisa cu litere mici
    else {
      if (
        websiteSection != websiteSection.toLowerCase() &&
        !cacheObj[websiteSection.toLowerCase()]
      ) {
        cacheObj[websiteSection.toLowerCase()] = 1;
      } else {
        cacheObj[websiteSection.toLowerCase()]++;
      }
    }
  };

  cacheObj.getCache = function() {
    return this;
  };

  return cacheObj;
}

module.exports = {
  FIRST_NAME,
  LAST_NAME,
  GRUPA,
  initCaching
};
